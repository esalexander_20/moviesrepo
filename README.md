Movies from the moview DB

Librerias Utilizadas

- Alamofire

Es una Libreria especificida para Swift que nos permite realizar peticiones a un servidor web cumpliendo con requisitos previos de manejo de APIs y manejo de respuesta del servidor a peticiones HTTP o HTTPS

Maneras de uso:
***Archivo: constants
Se utiliza para definir una variable AlamofireSessionManager la cual se puede utilizar para configurar de manera global las peticiones que se realizaran al servidor, dando la posibilidad que se puede agregar headers ya establecidos y almacenados en una variable.
 let AlamofireSessionManager: Alamofire.SessionManager = {
    let configuration = URLSessionConfiguration.default
    configuration.httpAdditionalHeaders = headersLocal
    return Alamofire.SessionManager(configuration: configuration)
}()

***Archivo : Services
Se utiliza cuando se realiza la petición al servidor junto a los parametros necesarios para que dicha petición se realice de manera correcta, se agrega el parámetro de method el cual se utliza para definir que tipo de petición será si GET O POST.

La respuesta que se espera es, en este caso, de tipo Json y se declara en el formato de encoding que realizará el procesamiento la libreria de Alamofire.

AlamofireSessionManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                switch response.result{
                case .success:
                case .failure(let error):
                }
            }

- AlamofireImage
Es una extensión de la libreria de Alamofire que se utiliza para la carga de imagenes ya se de manera local agregandole efectos como por ejemplo de desvanecimiento o la carga de imagenes alojadas en un servidor web. La extensión administra la petición para la carga de la imagen y la mantiene en cache, ese proceso lo hace de manera default útil para la carga de multiples imagenes en una misma vista.

Maneras de uso:
*** Archivo: DetailMovieVC

Se utiliza para descargar la imagen del poster de la pelicula que se selecciona como detalle de cada uno de los elementos que contiene la pelicula seleccionada.

Se crea la variable imageURL la cual se forma con la url 
URL_BASE_IMG = http://image.tmdb.org/t/p/w154/

más el valor de posterPath que contiene cada pelicula ej: poster_path": "/bOFaAXmWWXC3Rbv4u4uM9ZSzRXP.jpg",

 if let imageURL = URL(string: "\(URL_BASE_IMG)\(movie.posterPath)"), let placeholder = UIImage(named: "splash") {
                self.imgPoster.af_setImage(withURL: imageURL, placeholderImage: placeholder)
 }

 *** Archivo : MiViewCell
 Se utiliza para la carga de la imagen del poster de la pelicula pero en la lista de peliculas mostradas al usuario ya sea la categoría NOW PLAYING o la categoría  POLULAR que se muestran en la pantalla principal 

- KDCircularProgress

- PromiseKit/Alamofire