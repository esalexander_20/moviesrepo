//
//  ItemCell.swift
//  MoviesDB
//
//  Created by Davivienda El Salvador on 18/8/21.
//

import UIKit
import KDCircularProgress

class ItemCell: UITableViewCell {

    @IBOutlet weak var imgMovie: UIImageView!
    @IBOutlet weak var titleMovie: UILabel!
    @IBOutlet weak var fechaMovie: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var circularProgress: CircularProgressView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configureCell(movie: Result){
        if let imageURL = URL(string: "\(URL_BASE_IMG)\(movie.posterPath)"), let placeholder = UIImage(named: "splash") {
            self.imgMovie.af_setImage(withURL: imageURL, placeholderImage: placeholder)
        }
        self.titleMovie.text = movie.title
        
        if let date = movie.releaseDate{
            if !date.isEmpty{
                let releaseDateFormater = date.getFormatteDate(fromDate: date,formatter: "MMM dd,yyyy")
                self.fechaMovie.text = releaseDateFormater
            }else{
                self.fechaMovie.text = "Unknow"
            }
            
        }else{
            self.fechaMovie.text = "Unknow"
        }
        let averageValueLabel = (movie.voteAverage/10) * 100
        let averageValueProgress = (movie.voteAverage/10)

        let porcentageValue = Float(averageValueProgress)
        circularProgress.trackColor = UIColor.lightGray
        circularProgress.setProgressWithAnimation(duration: 1.0, value: porcentageValue)
        circularProgress.progressColor =   averageValueLabel >= 50 ?  .green : .yellow
        valueLbl.text = "\(Int(averageValueLabel)) %"
    }
    
}
