//
//  MiViewCell.swift
//  MoviesDB
//
//  Created by Davivienda El Salvador on 19/8/21.
//

import Foundation
import UIKit

protocol MiViewCellDelegate {
    func miViewCell(_ miViewCell: MiViewCell, shouldMoveToDetailWith movie: Result)
}

class MiViewCell : UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    var localmovies = [Result]()

    var delegate: MiViewCellDelegate?

    func configure(moviesArray : [Result]){
        self.localmovies = moviesArray
        collectionView.delegate = self
        collectionView.dataSource = self
    }
}

extension MiViewCell: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.localmovies.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectioncell", for: indexPath) as? MiCollectionCell{
            let movieSelected = self.localmovies[indexPath.row]
            if let imageURL = URL(string: "\(URL_BASE_IMG)\(movieSelected.posterPath)"), let placeholder = UIImage(named: "splash") {
                cell.posterImg.af_setImage(withURL: imageURL, placeholderImage: placeholder)
            }
            return cell
        }else{
            let cell = UICollectionViewCell()
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.miViewCell(self, shouldMoveToDetailWith: localmovies[indexPath.row])
    }
}

