//
//  HeaderCell.swift
//  MoviesDB
//
//  Created by Davivienda El Salvador on 20/8/21.
//

import Foundation
import UIKit

class HeaderCell: UITableViewCell {
    @IBOutlet weak var headerLbl: UILabel!
}
