//
//  Constants.swift
//  MoviesDB
//
//  Created by Davivienda El Salvador on 18/8/21.
//

import Foundation
import Alamofire

var URL_BASE : String {
    return "https://api.themoviedb.org/3/movie/"
}

var URL_BASE_IMG : String{
    return "http://image.tmdb.org/t/p/w154/"
}

var URL_GENRE : String{
    return "https://api.themoviedb.org/3/genre/movie/"
}

let api_key = "55957fcf3ba81b137f8fc01ac5a31fb5"

 let headersLocal: HTTPHeaders = [
     "Accept": "application/json",
     "Content-Type" : "application/json"
 ]
 
 let AlamofireSessionManager: Alamofire.SessionManager = {
    let configuration = URLSessionConfiguration.default
    configuration.httpAdditionalHeaders = headersLocal
    return Alamofire.SessionManager(configuration: configuration)
}()

typealias movieResponseCompletion = (Movie?) -> Void

typealias generoResponseCompletion = (Genero?) -> Void
