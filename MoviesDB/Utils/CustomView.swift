//
//  CustomView.swift
//  MoviesDB
//
//  Created by Davivienda El Salvador on 19/8/21.
//

import Foundation
import UIKit

class CustomView: UIView{
    override func awakeFromNib() {
        setupUI()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupUI()
    }
    
    func setupUI(){
        self.layer.cornerRadius = 10
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 5
    }
}
