//
//  Services.swift
//  MoviesDB
//
//  Created by Davivienda El Salvador on 18/8/21.
//

import Foundation
import Alamofire
import PromiseKit

class Services {

    func gettingTypeOfMovies(url: String) -> Promise<Movie>{
        let url = URL(string: "\(URL_BASE)\(url)?api_key=\(api_key)")!

        return Promise<Movie>{ resolver in
            AlamofireSessionManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                switch response.result{
                case .success:
                    if let error = response.result.error{
                        debugPrint(error.localizedDescription)
                        resolver.reject(error)
                        return
                    }
                    if let data = response.data{
                        do{
                            if response.result.isSuccess{
                                let json = try JSONDecoder().decode(Movie.self, from: data)
                                resolver.fulfill(json)
                            }else{
                                if let error = response.error{
                                    resolver.reject(error)
                                }
                            }
                        }catch{
                            resolver.reject(error)
                        }
                    }
                case .failure(let error):
                    resolver.reject(error)
                }
            }
        }
    }
    
    func gettingPopularMovies(url: String, numberPage : Int) -> Promise<Movie>{
        var localUrl = "\(URL_BASE)\(url)"
        var localParams = ""
        let paratemers = [
            "api_key" : api_key,
            "page" : "\(numberPage)",
            "languge" : "en-US"
        ]
        for (key, value) in paratemers
        {
            localParams += key + "=" + value + "&"
        }
        
        if !localParams.isEmpty{
            localParams = "?" + localParams
            if localParams.hasSuffix("&"){
                localParams.removeLast()
            }
            
            localUrl = localUrl + localParams
        }
        
        let url = URL(string: "\(localUrl)")!
        
        return Promise<Movie>{ resolver in
            AlamofireSessionManager.request(url, method: .get, encoding: JSONEncoding.default).responseJSON { (response) in
                switch response.result{
                case .success:
                    if let error = response.result.error{
                        debugPrint(error.localizedDescription)
                        resolver.reject(error)
                        return
                    }
                    if let data = response.data{
                        do{
                            if response.result.isSuccess{
                                let json = try JSONDecoder().decode(Movie.self, from: data)
                                resolver.fulfill(json)
                            }else{
                                if let error = response.error{
                                    resolver.reject(error)
                                }
                            }
                        }catch{
                            resolver.reject(error)
                        }
                    }
                case .failure(let error):
                    resolver.reject(error)
                }
            }
        }
    }
    
    func gettingGenreMovies(url: String , completion: @escaping generoResponseCompletion){
        guard let url = URL(string: "\(URL_GENRE)\(url)?api_key=\(api_key)") else {return}
        debugPrint("Url a utilizar  \(url)")
        AlamofireSessionManager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result{
            case .success:
                if let error = response.result.error{
                    debugPrint(error.localizedDescription)
                    completion(nil)
                    return
                }
                guard let data = response.data else {return completion(nil)}
                do{
                    let json = try JSONDecoder().decode(Genero.self, from: data)
                    completion(json)
                }catch{
                    debugPrint(error.localizedDescription)
                    completion(nil)
                }
                
            case .failure(let error):
                debugPrint(error.localizedDescription)
                completion(nil)
            }
        }
    }
}
