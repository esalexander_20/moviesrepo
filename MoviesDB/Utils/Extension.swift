//
//  Extension.swift
//  MoviesDB
//
//  Created by Davivienda El Salvador on 25/8/21.
//

import Foundation

extension String {
    func getFormatteDate(fromDate: String, formatter: String) -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatter

        let date: Date? = dateFormatterGet.date(from: fromDate)
        return dateFormatter.string(from: date!)
    }
}
