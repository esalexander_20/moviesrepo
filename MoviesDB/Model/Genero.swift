//
//  Genero.swift
//  MoviesDB
//
//  Created by Davivienda El Salvador on 19/8/21.
//

import Foundation

struct Genero: Codable {
    let genres: [Genre]
}

struct Genre: Codable {
    let id: Int
    let name: String
}
