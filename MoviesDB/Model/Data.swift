//
//  Data.swift
//  MoviesDB
//
//  Created by Davivienda El Salvador on 20/8/21.
//

import Foundation

class Data {
    var header : String?
    var peliculas : [Result]?
    
    init(header: String, peliculas: [Result]  ) {
        self.header = header
        self.peliculas = peliculas
    }
}
