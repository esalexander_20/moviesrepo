//
//  Movie.swift
//  MoviesDB
//
//  Created by Davivienda El Salvador on 18/8/21.
//

import Foundation


import Foundation

struct Movie: Codable {
    let page: Int
    let results: [Result]
    let totalPages, totalResults: Int

    enum CodingKeys: String, CodingKey {
        case page, results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}

struct Result: Codable {
    let adult: Bool
    let genreIDS: [Int]
    let id: Int
    let overview: String
    let posterPath, title: String
    let releaseDate : String?
    let voteAverage: Double
    let voteCount: Int

    enum CodingKeys: String, CodingKey {
        case adult
        case genreIDS = "genre_ids"
        case id
        case overview
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case title
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
}


