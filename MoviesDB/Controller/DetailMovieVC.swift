//
//  DetailMovieVC.swift
//  MoviesDB
//
//  Created by Davivienda El Salvador on 18/8/21.
//

import UIKit
import AlamofireImage

class DetailMovieVC: UIViewController {

    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var btnCerrar: UIButton!
    @IBOutlet weak var titleMovie: UILabel!
    @IBOutlet weak var dateMovie: UILabel!
    @IBOutlet weak var overViewMovie: UILabel!
    @IBOutlet weak var genreCollection: UICollectionView!
    
    var movie : Result!
    var api  = Services()
    var generosID = [Int]()
    var generosName = [String]()
    var generos = [Genre]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI(){
        self.navigationItem.setHidesBackButton(true, animated: false)
        if let movie = self.movie {
            if let imageURL = URL(string: "\(URL_BASE_IMG)\(movie.posterPath)"), let placeholder = UIImage(named: "splash") {
                self.imgPoster.af_setImage(withURL: imageURL, placeholderImage: placeholder)
            }
            
            titleMovie.text = movie.title
            if let date = movie.releaseDate{
                if !date.isEmpty{
                    let releaseDateFormater = date.getFormatteDate(fromDate: date,formatter: "MMM dd,yyyy")
                    self.dateMovie.text = releaseDateFormater
                }else{
                    self.dateMovie.text = "Unknow"
                }
            }else{
                dateMovie.text = "Unknow"
            }
            overViewMovie.text = movie.overview
            generosID = movie.genreIDS
            self.gettingGeneros()
        }
        
    }
    
    func gettingGeneros(){
        api.gettingGenreMovies(url: "list") { AnswerGenre in
            if let response = AnswerGenre{
                self.generos = response.genres
                debugPrint("Generos \(self.generos)")
                self.addingGenreLabels()
            }
        }
    }
    
    func addingGenreLabels(){
        if self.generosID.count > 0{
            for genre in generosID{
                var genreLbl = ""
                let genreFilters = generos.filter{ ($0.id == genre) }.map({$0.name})
                if genreFilters.count > 0 {
                    genreLbl = genreFilters[0]
                    debugPrint("genero \(genreLbl)")
                    self.generosName.append(genreLbl.uppercased())
                }else {
                    debugPrint("No se Encontro nombre de Banco")
                    
                }
            }
            self.genreCollection.reloadData()
        }
    }
    
    @IBAction func btnClosePressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


class GenreCollectionView: UICollectionViewCell {
    @IBOutlet weak var genreNameLbl: UILabel!
    
}

extension DetailMovieVC : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return generosName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "genrecollectioncell", for: indexPath) as? GenreCollectionView{
            cell.genreNameLbl.text = generosName[indexPath.row]
            return cell
        }else{
            let cell = UICollectionViewCell()
            return cell
        }
    }
    
    
}
