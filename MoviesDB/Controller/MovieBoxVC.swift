//
//  MovieBoxVC.swift
//  MoviesDB
//
//  Created by Davivienda El Salvador on 18/8/21.
//

import UIKit

class MovieBoxVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var moviesData = [Data]()
    
    var moviesNowPlayings = [Result]()
    var moviesMostPopular = [Result]()
    
    let movieApi = Services()
    
    var pageNumberParameter = 1
    var limit = 0
    var totalEntries = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gettingMoviesPlayingNow()
        setupUI()
    }
    
    func setupUI(){
        let nib = UINib.init(nibName: "ItemCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "itemcell")
        
    }
    
    func gettingMoviesPlayingNow(){
        let response = movieApi.gettingTypeOfMovies(url: "now_playing")
        response.done { Movie in
            self.moviesNowPlayings = Movie.results
            self.moviesData.append(Data.init(header: "Now PLaying", peliculas: self.moviesNowPlayings))
            self.tableView.reloadData()
            self.gettingMoviesMostPopular(numberPage: self.pageNumberParameter)
        }.catch { Error in
            debugPrint("error \(Error.localizedDescription)")
        }
    }
    
    func gettingMoviesMostPopular(numberPage : Int){
        let response = movieApi.gettingPopularMovies(url: "popular", numberPage: numberPage)
        response.done { Movie in
            self.moviesMostPopular = Movie.results
            if numberPage == 1 {
                self.moviesData.append(Data.init(header: "Most Popular", peliculas: self.moviesMostPopular))
                self.limit = self.moviesMostPopular.count
                self.totalEntries = Movie.totalPages
            }else{
                self.moviesMostPopular = Movie.results
                self.moviesData[1].peliculas?.append(contentsOf: self.moviesMostPopular)
            }
            self.tableView.reloadData()
            
        }.catch { Error in
            debugPrint("error \(Error.localizedDescription)")
        }
        
    }
}

extension MovieBoxVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        headerView.backgroundColor = .darkGray
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = moviesData[section].header
        label.font = .systemFont(ofSize: 18, weight: .bold)
        label.textColor = .systemOrange
        
        headerView.addSubview(label)
        
        return headerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let movies = moviesData[section].peliculas else { return 0 }
        if section == 0 {
            return 1
        }
        return movies.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return moviesData.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return moviesData[section].header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if let peliculas = moviesData[indexPath.section].peliculas{
                let cell = tableView.dequeueReusableCell(withIdentifier: "miviewcell", for: indexPath) as! MiViewCell
                cell.configure(moviesArray: peliculas)
                cell.delegate = self
                return cell
            }else{
                return UITableViewCell()
            }
        }else{
            if let peliculas = moviesData[indexPath.section].peliculas{
                let cell = tableView.dequeueReusableCell(withIdentifier: "itemcell", for: indexPath) as! ItemCell
                let movie = peliculas[indexPath.row]
                cell.configureCell(movie: movie)
                return cell
            }else{
                return  UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section != 0{
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            
            if indexPath.row == lastRowIndex && pageNumberParameter < totalEntries {
                pageNumberParameter += 1
                gettingMoviesMostPopular(numberPage: pageNumberParameter)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let peliculas = moviesData[indexPath.section].peliculas{
            let movie = peliculas[indexPath.row]
            moveToDetail(with: movie)
        }
        
    }
    
    fileprivate func moveToDetail(with movie: Result) {
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailMovieVC") as! DetailMovieVC
        detailVC.movie = movie
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}


extension MovieBoxVC: MiViewCellDelegate {
    func miViewCell(_ miViewCell: MiViewCell, shouldMoveToDetailWith movie: Result) {
        moveToDetail(with: movie)
    }
}
